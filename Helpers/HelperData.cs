﻿using Microsoft.ML;
using MLTest02ML.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MLTest02.Helpers
{
    public static class HelperData
    {
        private static string DATA_FILEPATH = @"C:\git\MLTest02\GooglePlayDatasets\fifa_2020-test.csv";
        //private static string DATA_FILEPATH = @"C:\Users\jblomstrand\AppData\Local\Temp\29a9eec0-8be7-489a-a26b-3a9c6df38419.tsv";

        public static List<ModelInput> GetData()
        {
            return GetDataSamples(DATA_FILEPATH);
        }

        public static List<ModelInput> GetData(int size)
        {
            return GetDataSamples(DATA_FILEPATH, size);
        }

        private static List<ModelInput> GetDataSamples(string dataFilePath, int size = 0)
        {
            // Create MLContext
            MLContext mlContext = new MLContext();

            // Load dataset
            IDataView dataView = mlContext.Data.LoadFromTextFile<ModelInput>(
                                            path: dataFilePath,
                                            hasHeader: true,
                                            separatorChar: ';',
                                            allowQuoting: true,
                                            allowSparse: false);

            IEnumerable<ModelInput> sampleForPrediction = mlContext.Data.CreateEnumerable<ModelInput>(dataView, false);

            if (size != 0)
                sampleForPrediction = sampleForPrediction.Take(size);

            return sampleForPrediction.ToList();
        }
    }
}
