﻿using MLTest02ML.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTest02.Models
{
    public class Team
    {
        public float Budget { get; set; }
        public List<ModelInput> Players { get; set; } = new List<ModelInput>();

        public float GetTeamValue()
        {
            float totalValue = 0f;

            if(Players != null)
                Players.ForEach(x => totalValue += x.Value_eur);

            return totalValue;
        }
    }
}
