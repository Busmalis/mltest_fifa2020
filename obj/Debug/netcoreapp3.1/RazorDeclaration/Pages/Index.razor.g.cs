#pragma checksum "C:\git\MLTest02\Pages\Index.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0c3db81514a69dbe7dadb14118dff2b35f261391"
// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace MLTest02.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\git\MLTest02\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\git\MLTest02\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\git\MLTest02\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\git\MLTest02\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\git\MLTest02\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\git\MLTest02\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\git\MLTest02\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\git\MLTest02\_Imports.razor"
using MLTest02;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\git\MLTest02\_Imports.razor"
using MLTest02.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\git\MLTest02\Pages\Index.razor"
using MLTest02ML.Model;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\git\MLTest02\Pages\Index.razor"
using MLTest02.Helpers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\git\MLTest02\Pages\Index.razor"
using MLTest02.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\git\MLTest02\Pages\Index.razor"
using MLTest02.Extensions;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\git\MLTest02\Pages\Index.razor"
using System.Globalization;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/")]
    public partial class Index : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 93 "C:\git\MLTest02\Pages\Index.razor"
       

    public List<ModelInput> tableData = new List<ModelInput>();
    public Formation_4231 teamData = new Formation_4231();

    protected override async Task OnInitializedAsync()
    {
        var test = HelperData.GetData().Where(x => x.Potential > 80 && x.Player_positions != "GK").OrderBy(x => x.Age).Take(10).ToList();

        teamData = HelperTeamGenerator.CreateNewTeam(150000000f);

        tableData = test.OrderByDescending(x => (GetPredFloat(x) - x.Value_eur)).ToList();

    }

    private float GetDiffValue(ModelInput data)
    {
        var value = (GetPredFloat(data) - data.Value_eur);

        return value;
    }

    private string GetPred()
    {

        //// Add input data
        var input = new ModelInput()
        {

        };

        //// Load model and predict output of sample data
        ModelOutput result = ConsumeModel.Predict(input);

        return result.PredictedValue.ToString();
    }

    private string GetPred(ModelInput item)
    {

        //// Load model and predict output of sample data
        ModelOutput result = ConsumeModel.Predict(item);

        return result.PredictedValue.toEuroString();
    }

    private float GetPredFloat(ModelInput item)
    {

        //// Load model and predict output of sample data
        ModelOutput result = ConsumeModel.Predict(item);

        return result.PredictedValue;
    }


#line default
#line hidden
#nullable disable
    }
}
#pragma warning restore 1591
