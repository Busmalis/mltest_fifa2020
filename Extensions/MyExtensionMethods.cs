﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace MLTest02.Extensions
{
    public static class MyExtensionMethods
    {
        static CultureInfo cultureInfo = CultureInfo.GetCultureInfo("da-DE");

        public static string toEuroString(this float f)
        {
            return String.Format(cultureInfo, "{0:C}", f);
        }
    }
}
