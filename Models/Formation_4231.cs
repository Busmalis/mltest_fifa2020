﻿using MLTest02ML.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTest02.Models
{
    public class Formation_4231 : Team
    {
        public ModelInput GK { get; set; }

        public ModelInput LB { get; set; }
        public ModelInput RB { get; set; }
        public ModelInput CB1 { get; set; }
        public ModelInput CB2 { get; set; }

        public ModelInput CMD1 { get; set; }
        public ModelInput CMD2 { get; set; }
                
        public ModelInput LM { get; set; }
        public ModelInput CM { get; set; }
        public ModelInput RM { get; set; }

        public ModelInput ST { get; set; }

    }
}
