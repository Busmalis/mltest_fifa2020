﻿using Microsoft.ML;
using MLTest02.Models;
using MLTest02ML.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTest02.Helpers
{
    public static class HelperTeamGenerator
    {
        private enum POSITION
        {
            GK,
            LB,
            RB,
            CB,
            CDM,
            LM,
            RM,
            CM,
            ST
        }

        private static string DATA_FILEPATH = @"C:\git\MLTest02\GooglePlayDatasets\fifa_2020-complete.csv";

        private static float _teamValue = 0f;
        private static Formation_4231 _team = new Formation_4231();

        public static Formation_4231 CreateNewTeam(float budget)
        {
            _team = new Formation_4231();
            _team.Budget = budget;

            _team.GK = GetPlayerByPosition(POSITION.GK);
            _team.Players.Add(_team.GK);

            _team.LB = GetPlayerByPosition(POSITION.LB);
            _team.Players.Add(_team.LB);
            _team.CB1 = GetPlayerByPosition(POSITION.CB);
            _team.Players.Add(_team.CB1);
            _team.CB2 = GetPlayerByPosition(POSITION.CB);
            _team.Players.Add(_team.CB2);
            _team.RB = GetPlayerByPosition(POSITION.RB);
            _team.Players.Add(_team.RB);

            _team.CMD1 = GetPlayerByPosition(POSITION.CDM);
            _team.Players.Add(_team.CMD1);
            _team.CMD2 = GetPlayerByPosition(POSITION.CDM);
            _team.Players.Add(_team.CMD2);

            _team.LM = GetPlayerByPosition(POSITION.LM);
            _team.Players.Add(_team.LM);
            _team.CM = GetPlayerByPosition(POSITION.CM);
            _team.Players.Add(_team.CM);
            _team.RM = GetPlayerByPosition(POSITION.RM);
            _team.Players.Add(_team.RM);

            _team.ST = GetPlayerByPosition(POSITION.ST);
            _team.Players.Add(_team.ST);

            _teamValue = _team.GetTeamValue();

            return _team;
        }

        private static ModelInput GetPlayerByPosition(POSITION position)
        {
            List<ModelInput> data = GetPlayersByPosition(position).Where(x => (x.Value_eur < ((_team.Budget - _team.GetTeamValue()) / (11 - _team.Players.Count))) && x.Age < 28 && x.Value_eur > 0).OrderByDescending(x => x.Overall).ThenBy(x => x.Potential).ThenBy(x => x.Value_eur).ToList();
            List<ModelInput> temp = data;

            foreach (var item in _team.Players)
            {
                var p = data.SingleOrDefault(x => x.Sofifa_id == item.Sofifa_id);
                if(data.Contains(p))
                    temp = data.Where(x => x.Sofifa_id != p.Sofifa_id).ToList();
            }

            var value = temp.First();

            return value;
        }

        private static List<ModelInput> GetPlayersByPosition(POSITION position)
        {
            // Create MLContext
            MLContext mlContext = new MLContext();

            // Load dataset
            IDataView dataView = mlContext.Data.LoadFromTextFile<ModelInput>(
                                            path: DATA_FILEPATH,
                                            hasHeader: true,
                                            separatorChar: ';',
                                            allowQuoting: true,
                                            allowSparse: false);

            IEnumerable<ModelInput> data = mlContext.Data.CreateEnumerable<ModelInput>(dataView, false).Where(x => x.Player_positions.Contains(GetValueByPosition(position)));

            return data.ToList();
        }

        private static string GetValueByPosition(POSITION position)
        {
            string value = string.Empty;

            switch (position)
            {
                case POSITION.GK:
                    value = "GK";
                    break;
                case POSITION.LB:
                    value = "LB";
                    break;
                case POSITION.RB:
                    value = "RB";
                    break;
                case POSITION.CB:
                    value = "CB";
                    break;
                case POSITION.CDM:
                    value = "CDM";
                    break;
                case POSITION.LM:
                    value = "LM";
                    break;
                case POSITION.RM:
                    value = "RM";
                    break;
                case POSITION.CM:
                    value = "CM";
                    break;
                case POSITION.ST:
                    value = "ST";
                    break;
                default:
                    break;
            }

            return value;
        }
    }
}
